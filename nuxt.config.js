import colors from 'vuetify/es5/util/colors';

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: 'Peer To Peer Lending In India | Borrow Or Lend Money Online With P2P Lending - Lendbox' || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      // { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
      //SEO
      { name: "copyright", content: "Transactree Technologies Pvt. Ltd." },
      { httpEquiv: 'X-UA-Compatible', content: 'IE=edge' },
      { name: "description", content: "India's leading Peer to peer lending platform with RBI's NBFC-P2P licence. Lend money online to earn high returns or get instant personal loans in India. Trusted by 2,00,000+ users." },
      { name: "keywords", content: "peer to peer lending, p2p lending, p2p lending india, peer to peer lending india, best p2p lending platform,,Lending india, private money lenders,lend money online, borrow money online,instant loans online, p2p loans" },
      { name: "google-site-verification", content: "dqnPYAksnsshO0x2z0UzaOCqRJyhVjzNYFuDMXgjrU0" },
      { name: "geo.region", content: "IN" },
      { content: 'website', property: 'og:type' },
      { content: 'Peer to peer lending India | Personal Loans & Alternative Investing - Lendbox', property: 'og:title' },
      { content: 'https://www.lendbox.in', property: 'og:url' },
      { content: 'https://media.licdn.com/media/AAEAAQAAAAAAAAVYAAAAJGZlYWUzNWQwLTZjMWUtNDk4My1iY2UwLTJjZWM1M2FjNDIwZg.png', property: 'og:image' },
      { content: 'Borrow or Invest money with Lendbox, a trusted and leading Indian p2p lending platform licenced by RBI disrupting traditional banking system with technology', property: 'og:description' },
      { content: 'Lendbox', property: 'og:site_name' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },

      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Muli&display=swap' },
      {
        rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
      }
    ],
    script: [
      { src: "https://wchat.freshchat.com/js/widget.js" },
      //SEO links
      { rel: "canonical", href: "https://www.lendbox.in" },
      { property: "fb:pages", content: "641410082662094" },
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  
    /**
   * WebFont Loader
   */
  webfontloader: {
    google: {
      families: ['Muli:400,500,600,700,800'] //Loads font with weights
    }
  },

  /** Global CSS
  */
  css: [
    '~/assets/style/main.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/core-components.js',
    '@/plugins/filters.js',
    { src: '@/plugins/gauge.js', ssr: false },
    { src: '@/plugins/vue-carousel.js', ssr: false },
    { src: '@/plugins/vue-charts.js', ssr: false },
    { src: "@/plugins/twitter.js", ssr: false },
    { src: "@/plugins/facebook.js", ssr: false },
    //SEO
    { src: '@/plugins/SEOSocial.json', ssr: false },
    // { src: '@/plugins/googleanalytics.js', ssr: false },

    //SEO ends


  ],
  /*
  ** Nuxt.js dev-modules
  */
  devModules: [
    '@nuxtjs/vuetify',
  ],
  // transpileDependencies: [
  //   'vue-echarts',
  //   'resize-detector'
  // ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    ['@nuxtjs/google-analytics', {
      id: 'UA-36251023-1'
    }]
  ],
  axios: {
    // proxyHeaders: false
    cors: true,
    https: true,
    credentials: true
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: ['vue-echarts', 'resize-detector'],
    vendor: ['vue-svg-gauge'],

    /*
    ** You can extend webpack config here
    */
    // extend(config, ctx) {
    // }
  }
}
