import Vue from 'vue'
import { getMatchedComponentsInstances, promisify, globalHandleError } from './utils'
import NuxtLoading from './components/nuxt-loading.vue'

import '../assets/style/main.css'

import '../node_modules/vuetify/src/styles/main.sass'

import _4d70095f from '../layouts/homeLayout.vue'
import _6f6c098b from './layouts/default.vue'

const layouts = { "_homeLayout": _4d70095f,"_default": _6f6c098b }

export default {
  head: {"titleTemplate":"%s - static-webapp","title":"Peer To Peer Lending In India | Borrow Or Lend Money Online With P2P Lending - Lendbox","meta":[{"charset":"utf-8"},{"name":"viewport","content":"width=device-width, initial-scale=1"},{"name":"copyright","content":"Transactree Technologies Pvt. Ltd."},{"httpEquiv":"X-UA-Compatible","content":"IE=edge"},{"name":"description","content":"India's leading Peer to peer lending platform with RBI's NBFC-P2P licence. Lend money online to earn high returns or get instant personal loans in India. Trusted by 2,00,000+ users."},{"name":"keywords","content":"peer to peer lending, p2p lending, p2p lending india, peer to peer lending india, best p2p lending platform,,Lending india, private money lenders,lend money online, borrow money online,instant loans online, p2p loans"},{"name":"google-site-verification","content":"dqnPYAksnsshO0x2z0UzaOCqRJyhVjzNYFuDMXgjrU0"},{"name":"geo.region","content":"IN"},{"content":"website","property":"og:type"},{"content":"Peer to peer lending India | Personal Loans & Alternative Investing - Lendbox","property":"og:title"},{"content":"https:\u002F\u002Fwww.lendbox.in","property":"og:url"},{"content":"https:\u002F\u002Fmedia.licdn.com\u002Fmedia\u002FAAEAAQAAAAAAAAVYAAAAJGZlYWUzNWQwLTZjMWUtNDk4My1iY2UwLTJjZWM1M2FjNDIwZg.png","property":"og:image"},{"content":"Borrow or Invest money with Lendbox, a trusted and leading Indian p2p lending platform licenced by RBI disrupting traditional banking system with technology","property":"og:description"},{"content":"Lendbox","property":"og:site_name"}],"link":[{"rel":"icon","type":"image\u002Fx-icon","href":"\u002Ffavicon.ico"},{"rel":"stylesheet","href":"https:\u002F\u002Ffonts.googleapis.com\u002Fcss?family=Muli&display=swap"},{"rel":"stylesheet","href":"https:\u002F\u002Fstackpath.bootstrapcdn.com\u002Ffont-awesome\u002F4.7.0\u002Fcss\u002Ffont-awesome.min.css"},{"rel":"stylesheet","type":"text\u002Fcss","href":"https:\u002F\u002Ffonts.googleapis.com\u002Fcss?family=Roboto:100,300,400,500,700,900&display=swap"},{"rel":"stylesheet","type":"text\u002Fcss","href":"https:\u002F\u002Fcdn.jsdelivr.net\u002Fnpm\u002F@mdi\u002Ffont@latest\u002Fcss\u002Fmaterialdesignicons.min.css"}],"script":[{"src":"https:\u002F\u002Fwchat.freshchat.com\u002Fjs\u002Fwidget.js"},{"rel":"canonical","href":"https:\u002F\u002Fwww.lendbox.in"},{"property":"fb:pages","content":"641410082662094"}],"style":[]},

  render(h, props) {
    const loadingEl = h('NuxtLoading', { ref: 'loading' })
    const layoutEl = h(this.layout || 'nuxt')
    const templateEl = h('div', {
      domProps: {
        id: '__layout'
      },
      key: this.layoutName
    }, [ layoutEl ])

    const transitionEl = h('transition', {
      props: {
        name: 'layout',
        mode: 'out-in'
      },
      on: {
        beforeEnter(el) {
          // Ensure to trigger scroll event after calling scrollBehavior
          window.$nuxt.$nextTick(() => {
            window.$nuxt.$emit('triggerScroll')
          })
        }
      }
    }, [ templateEl ])

    return h('div', {
      domProps: {
        id: '__nuxt'
      }
    }, [loadingEl, transitionEl])
  },
  data: () => ({
    isOnline: true,
    layout: null,
    layoutName: ''
  }),
  beforeCreate() {
    Vue.util.defineReactive(this, 'nuxt', this.$options.nuxt)
  },
  created() {
    // Add this.$nuxt in child instances
    Vue.prototype.$nuxt = this
    // add to window so we can listen when ready
    if (process.client) {
      window.$nuxt = this
      this.refreshOnlineStatus()
      // Setup the listeners
      window.addEventListener('online', this.refreshOnlineStatus)
      window.addEventListener('offline', this.refreshOnlineStatus)
    }
    // Add $nuxt.error()
    this.error = this.nuxt.error
    // Add $nuxt.context
    this.context = this.$options.context
  },

  mounted() {
    this.$loading = this.$refs.loading
  },
  watch: {
    'nuxt.err': 'errorChanged'
  },

  computed: {
    isOffline() {
      return !this.isOnline
    }
  },
  methods: {
    refreshOnlineStatus() {
      if (process.client) {
        if (typeof window.navigator.onLine === 'undefined') {
          // If the browser doesn't support connection status reports
          // assume that we are online because most apps' only react
          // when they now that the connection has been interrupted
          this.isOnline = true
        } else {
          this.isOnline = window.navigator.onLine
        }
      }
    },
    async refresh() {
      const pages = getMatchedComponentsInstances(this.$route)

      if (!pages.length) {
        return
      }
      this.$loading.start()
      const promises = pages.map(async (page) => {
        const p = []

        if (page.$options.fetch) {
          p.push(promisify(page.$options.fetch, this.context))
        }
        if (page.$options.asyncData) {
          p.push(
            promisify(page.$options.asyncData, this.context)
              .then((newData) => {
                for (const key in newData) {
                  Vue.set(page.$data, key, newData[key])
                }
              })
          )
        }
        return Promise.all(p)
      })
      try {
        await Promise.all(promises)
      } catch (error) {
        this.$loading.fail()
        globalHandleError(error)
        this.error(error)
      }
      this.$loading.finish()
    },

    errorChanged() {
      if (this.nuxt.err && this.$loading) {
        if (this.$loading.fail) this.$loading.fail()
        if (this.$loading.finish) this.$loading.finish()
      }
    },

    setLayout(layout) {
      if (!layout || !layouts['_' + layout]) {
        layout = 'default'
      }
      this.layoutName = layout
      this.layout = layouts['_' + layout]
      return this.layout
    },
    loadLayout(layout) {
      if (!layout || !layouts['_' + layout]) {
        layout = 'default'
      }
      return Promise.resolve(layouts['_' + layout])
    }
  },
  components: {
    NuxtLoading
  }
}
