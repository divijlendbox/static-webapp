import Vue from 'vue'
import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from '../layouts/error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'

/* Plugins */

import nuxt_plugin_plugin_7d4d0a0f from 'nuxt_plugin_plugin_7d4d0a0f' // Source: ./vuetify/plugin.js (mode: 'all')
import nuxt_plugin_googleanalytics_1765759e from 'nuxt_plugin_googleanalytics_1765759e' // Source: ./google-analytics.js (mode: 'client')
import nuxt_plugin_axios_0f8bcbd5 from 'nuxt_plugin_axios_0f8bcbd5' // Source: ./axios.js (mode: 'all')
import nuxt_plugin_corecomponents_33a6d667 from 'nuxt_plugin_corecomponents_33a6d667' // Source: ../plugins/core-components.js (mode: 'all')
import nuxt_plugin_filters_14a52510 from 'nuxt_plugin_filters_14a52510' // Source: ../plugins/filters.js (mode: 'all')
import nuxt_plugin_gauge_07cdc75c from 'nuxt_plugin_gauge_07cdc75c' // Source: ../plugins/gauge.js (mode: 'client')
import nuxt_plugin_vuecarousel_eb857b48 from 'nuxt_plugin_vuecarousel_eb857b48' // Source: ../plugins/vue-carousel.js (mode: 'client')
import nuxt_plugin_vuecharts_750b4c87 from 'nuxt_plugin_vuecharts_750b4c87' // Source: ../plugins/vue-charts.js (mode: 'client')
import nuxt_plugin_twitter_6da4eb98 from 'nuxt_plugin_twitter_6da4eb98' // Source: ../plugins/twitter.js (mode: 'client')
import nuxt_plugin_facebook_25a8aa06 from 'nuxt_plugin_facebook_25a8aa06' // Source: ../plugins/facebook.js (mode: 'client')
import nuxt_plugin_SEOSocial_3babb540 from 'nuxt_plugin_SEOSocial_3babb540' // Source: ../plugins/SEOSocial.json (mode: 'client')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)
// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render(h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true
      console.warn(`<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead`)
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>`
Vue.component(Nuxt.name, Nuxt)

// vue-meta configuration
Vue.use(Meta, {
  keyName: 'head', // the component option name that vue-meta looks for meta info on.
  attribute: 'data-n-head', // the attribute name vue-meta adds to the tags it observes
  ssrAttribute: 'data-n-head-ssr', // the attribute name that lets vue-meta know that meta info has already been server-rendered
  tagIDKeyName: 'hid' // the property name that vue-meta uses to determine whether to overwrite or append a tag
})

const defaultTransition = {"name":"page","mode":"out-in","appear":false,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

async function createApp(ssrContext) {
  const router = await createRouter(ssrContext)

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    router,

    nuxt: {
      defaultTransition,
      transitions: [ defaultTransition ],
      setTransitions(transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [ transitions ]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },
      err: null,
      dateErr: null,
      error(err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        const nuxt = this.nuxt || this.$options.nuxt
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) ssrContext.nuxt.error = err
        return err
      }
    },
    ...App
  }

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    route,
    next,
    error: app.nuxt.error.bind(app),

    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    ssrContext
  })

  const inject = function (key, value) {
    if (!key) throw new Error('inject(key, value) has no key provided')
    if (typeof value === 'undefined') throw new Error('inject(key, value) has no value provided')
    key = '$' + key
    // Add into app
    app[key] = value

    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) return
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Vue.prototype.hasOwnProperty(key)) {
        Object.defineProperty(Vue.prototype, key, {
          get() {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  // Plugin execution

  if (typeof nuxt_plugin_plugin_7d4d0a0f === 'function') {
    await nuxt_plugin_plugin_7d4d0a0f(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_googleanalytics_1765759e === 'function') {
    await nuxt_plugin_googleanalytics_1765759e(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_0f8bcbd5 === 'function') {
    await nuxt_plugin_axios_0f8bcbd5(app.context, inject)
  }

  if (typeof nuxt_plugin_corecomponents_33a6d667 === 'function') {
    await nuxt_plugin_corecomponents_33a6d667(app.context, inject)
  }

  if (typeof nuxt_plugin_filters_14a52510 === 'function') {
    await nuxt_plugin_filters_14a52510(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_gauge_07cdc75c === 'function') {
    await nuxt_plugin_gauge_07cdc75c(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_vuecarousel_eb857b48 === 'function') {
    await nuxt_plugin_vuecarousel_eb857b48(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_vuecharts_750b4c87 === 'function') {
    await nuxt_plugin_vuecharts_750b4c87(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_twitter_6da4eb98 === 'function') {
    await nuxt_plugin_twitter_6da4eb98(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_facebook_25a8aa06 === 'function') {
    await nuxt_plugin_facebook_25a8aa06(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_SEOSocial_3babb540 === 'function') {
    await nuxt_plugin_SEOSocial_3babb540(app.context, inject)
  }

  // If server-side, wait for async component to be resolved first
  if (process.server && ssrContext && ssrContext.url) {
    await new Promise((resolve, reject) => {
      router.push(ssrContext.url, resolve, () => {
        // navigated to a different route in router guard
        const unregister = router.afterEach(async (to, from, next) => {
          ssrContext.url = to.fullPath
          app.context.route = await getRouteData(to)
          app.context.params = to.params || {}
          app.context.query = to.query || {}
          unregister()
          resolve()
        })
      })
    })
  }

  return {
    app,

    router
  }
}

export { createApp, NuxtError }
