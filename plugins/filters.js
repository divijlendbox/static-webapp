import Vue from 'vue';
import moment from 'moment';

Vue.filter('localeMonth', value => {
    if (value) {
        return moment(value).format('MMM-YY');
    }
    return value;
});

Vue.filter('localeDate', value => {
    if (value) {
        // return new Date(value).toLocaleDateString();
        return moment(value, 'DD/MM/YYYY').format('DD MMM YYYY');
    }
    return '----';
});

Vue.filter('rate', value => {
    if (value) {
        return value.toString().concat('%');
    }
    return value;
});

Vue.filter('mn', value => {
    if (value) {
        // let result = 0;
        // let divisor = 1000000;
        // let suffix = 'Mn';
        // if (value / divisor < 1) {
        //     divisor = 100000;
        //     suffix = 'k';
        // }
        // if (value / divisor < 1) {
        //     divisor = 10000;
        //     suffix = 'k';
        // }
        // if (value / divisor < 1) {
        //     divisor = 1000;
        //     suffix = 'k';
        // }
        // if (value / divisor < 1) {
        //     divisor = 100;
        //     suffix = '';
        // }
        // result = (value / divisor).toFixed(2) + suffix;
        // return result;
        value = String(value).trim();
        
        if (value.length >= 6) {
            if (value.length >= 7) {

                if (value.length > 7) {
                    return value.slice(0, value.length-6) + " Mn";
                }
                else return value.slice(0, 1) + "Mn";
            }
            else return '0.' + value.slice(0, 2) + "Mn";
        }
        else return value;
    }
})
Vue.filter('decimalCurrency', value => {
    if (!value) {
        return '0';
    }
    let [rupees, paise] = Number(value).toFixed(2).toString().split('.');
    // let temp = parseFloat(value).toFixed().toString();
    let temp = rupees;
    let lastThree = temp.substring(temp.length - 3);
    let otherNumbers = temp.substring(0, temp.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    let res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + "." + paise;
    return res;
});

Vue.filter('currency', value => {
    if (!value) {
        return '0';
    }
    let temp = parseFloat(value).toFixed().toString();
    let lastThree = temp.substring(temp.length - 3);
    let otherNumbers = temp.substring(0, temp.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    let res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
    return res;
});


Vue.filter('capitalizeFirst', string => {
    if (string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }
    return '';
});


Vue.filter('lbConstant', string => {
    if (string) {
        if (string === 'SALARIED_EMP') return "Salaried";
        if (string === 'SELF_EMP_BUSINESS') return "Self Employeed ";
        if (string.startsWith('SS')) {
            return string.replace("SS", "Salary Slip ")
        }
        return string.split("_").map(item => (item.charAt(0).toUpperCase() + item.slice(1).toLowerCase())).join(" ");
    }
    return '';
});

/** 
 * Deprecated
 */
// Vue.filter('currency', value => {
//   if (!value) {
//     return '0';
//   }
//   let temp = value.toString().split('.');
//   let y = '';
//   if (temp.length > 1) {
//     y = temp[1];
//   }
//   let x = temp[0];
//   let lastThree = x.substring(x.length - 3);
//   let otherNumbers = x.substring(0, x.length - 3);
//   if (otherNumbers != '')
//     lastThree = ',' + lastThree;
//   let res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree.concat(y && ".".concat(y));
//   return 'Rs.' + res;
// });

Vue.directive('longpress', {
    bind: function (el, binding, vNode) {
        // Make sure expression provided is a function
        if (typeof binding.value !== 'function') {
            // Fetch name of component
            const compName = vNode.context.name
            // pass warning to console
            let warn = `[longpress:] provided expression '${binding.expression}' is not a function, but has to be`
            if (compName) { warn += `Found in component '${compName}' ` }

            console.warn(warn)
        }

        // Define variable
        let pressTimer = null

        // Define funtion handlers
        // Create timeout ( run function after 1s )
        let start = (e) => {

            if (e.type === 'click' && e.button !== 0) {
                return;
            }

            if (pressTimer === null) {
                pressTimer = setTimeout(() => {
                    // Run function
                    handler()
                }, 1000)
            }
        }

        // Cancel Timeout
        let cancel = (e) => {
            // Check if timer has a value or not
            if (pressTimer !== null) {
                clearTimeout(pressTimer)
                pressTimer = null
            }
        }
        // Run Function
        const handler = (e) => {
            binding.value(e)
        }

        // Add Event listeners
        el.addEventListener("mousedown", start);
        el.addEventListener("touchstart", start);
        // Cancel timeouts if this events happen
        el.addEventListener("click", cancel);
        el.addEventListener("mouseout", cancel);
        el.addEventListener("touchend", cancel);
        el.addEventListener("touchcancel", cancel);
    }
});

